#coding:utf-8
'''
date: 20180613 
version: 1.0
convert Oracle SQL to MySQL
python version : 2.7
'''
import os
def main():
    print("- - - Convert Oracle SQL to MySQL - - - ")

source_file = 'oracle_source.sql'
save_file = 'mysql_file.sql'
file_path = "./" + source_file 
print ("read file from " + source_file )
f = open( source_file, 'r')
result = list()
for line in f.readlines():
    line = line.strip()
    line = line.replace("\"", "`" )
    line = line.replace("NUMBER","DECIMAL")
    line = line.replace("VARCHAR2","VARCHAR")
    line = line.replace("BYTE","")
    if not len(line) or line.startswith('#'):
        continue
    elif line.startswith(')'):
        break
    
    #print (line)
    result.append(line)
result.append(");")
#print(result)
 
if ( os.path.isfile(file_path)):
    print("File exists! remove it ")
    os.remove(file_path)
else:
    print("File not found!")

open(save_file , 'w').write('%s' % '\n'.join(result) )
print ("save data to " + save_file)
print("- - - Program Stop - - - ")
